var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  var data = req.query;
  //console.log(data);

	var id, kat, kraj, naslov, posta, link, povzetek;
				 		id = data.id;
				 		kat = data.kategorija;
				 		kraj = data. kraj;
				 		naslov = data.naslov;
				 		posta = data.postnaStevilka;
				 		link = data.povezava;
				 		povzetek = data.povzetek;

	if( 
		kat===undefined || 
		kraj===undefined || 
		naslov===undefined || 
		posta===undefined || 
		link===undefined || 
		povzetek===undefined || 
		kat==='' || 
		kraj==='' || 
		naslov==='' || 
		posta==='' || 
		link==='' || 
		povzetek==='') {
		res.send('Napaka pri dodajanju novice!');
	}
	else{

		var maxID = 0;
		for(var i in noviceSpomin) {
			if(noviceSpomin[i].id > maxID) {
			  maxID = noviceSpomin[i].id;
			}
		}
		maxID += 1;

			noviceSpomin.push({
				id: maxID, 
				naslov: naslov, 
				povzetek: povzetek, 
				kategorija:kat, 
				postnaStevilka: posta, 
				kraj: kraj, 
        povezava: link
			});
			res.redirect('/');
		
	}

});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
  
  
  var data = req.query;
  
  
	var idNovice = data.id;
	//console.log(idNovice);

	if(idNovice===undefined || idNovice==='') {
		res.send('Napačna zahteva!');
	}
	else{

		var obstaja = false;
		for(var i in noviceSpomin) {
		  //console.log(noviceSpomin[i].id + " " + idNovice);
			if(noviceSpomin[i].id == idNovice) {
				obstaja = true;
				//console.log(obstaja);
				break;
			}
		}

		if(obstaja) {
			noviceSpomin.splice(i,1);
			res.redirect('/');
		}
		else {
			res.send("Novica z id-jem " + idNovice + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
			
		}
	}
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
